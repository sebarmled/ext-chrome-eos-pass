  EOSPass Login
===============

A chrome extension providing a client for EOSPass login protocol.

EOSPass system requires the user to register a public key with "eospass" permission.

The user enters his credentials : account name and the WIF private key of his eospass address


For now, the private key is stored in chrome storage system. Only accessible from this extension or from the user's machine file system.

INSTALLATION: 1) activate "developper mode" in Chrome, 2) then use the button "load unpacked"

License :
----------

 EOSPass Login (c) 2019 EOS Geneva - All Rights Reserved
 Unauthorized copying of this file, via any medium is strictly prohibited
 Proprietary and confidential

