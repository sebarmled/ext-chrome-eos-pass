/*
 EOSPass login content.js (c) 2019 EOS Geneva - All Rights Reserved
 Unauthorized copying of this file, via any medium is strictly prohibited
 Proprietary and confidential
*/

function PostPass(path, params) {
	// POST the login query to path
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("POST", path);
	xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	xmlhttp.onreadystatechange = function() {
		if (this.readyState === XMLHttpRequest.DONE ) {
			if (this.status === 200) {
				// User granted. Go to user page
				window.location.href="user.php";
			}
			if (this.status === 401) {
				var respmsg = JSON.parse(this.responseText).message;
				alert(
				"Login refused by the server.\n"
				+ "Message from server : " + respmsg
				);
				// Page reload to get a new nonce
				window.location.reload(true);
			}
		}
	}
	xmlhttp.send(JSON.stringify(params));
}

window.addEventListener("message", function(event) {
	// Listen to message from the web page
	// Filter message from this own extension
	if (event.source != window)
		return;
	if (event.data.action && (event.data.action == "SIGN")) {
		// get account data (key), make the signature and build the query
		chrome.storage.local.get(['pvKey','account'], function(data){
			var urimsg = event.data.text;
			var privateWif = data['pvKey'];
			var signature = eosjs_ecc.sign(urimsg, privateWif);
			var pubkey = eosjs_ecc.privateToPublic(privateWif);
			var loginpoint = urimsg.match(/\/[0-9a-z]+(\.[a-z]+)?/g)[1];
			PostPass(loginpoint, {'uri':urimsg, 'account_name':data['account'], 'signature':signature});
		});
	}
}, false);

function signreq(txt) {
	// Send a sign query from the web page to the Content Script
	window.postMessage({ action: "SIGN", text: txt }, "*");
};

// run through all links in the web page
var linkslist = document.getElementsByTagName('a');
for (var idx = 0; idx < linkslist.length; idx++) {
	if (linkslist[idx].href.startsWith("eospass:")) {
		// if a link to eospass protocol
		uri = linkslist[idx].href;
		linkslist[idx].addEventListener("click",function(e){
			e.preventDefault();
			// load and check account is set up
			chrome.storage.local.get('account', function(data){
				if (data['account'] == undefined) {
					alert("Please save your private key of your account in the EOSPass login extension.");
				}
				else {
					// check the eospass uri is for the same server, preventing cross-site hack
					var host = window.location.hostname;
					var urihost = uri.match(/eospass:\/\/(.+)\//);
					if ( host === urihost[1]) {
						if (window.confirm("Confirm login to "+host+" using EOSPass ?")) { 
							// send a message to this extension content script
							signreq(uri);
						}
					}
				}
			});
		},false);
	}
}

