/*
 EOSPass login popup.js (c) 2019 EOS Geneva - All Rights Reserved
 Unauthorized copying of this file, via any medium is strictly prohibited
 Proprietary and confidential
*/

document.addEventListener('DOMContentLoaded', function(event) {
	document.getElementById('saveKey').onclick = savekey;
	document.getElementById('delKey').onclick = removekey;
	chrome.storage.local.get(['account'], function(dataacc){
		if (dataacc['account'] != undefined) {
			dispname(dataacc['account']);
			hideinput();
		}
		else
			showinput();
	});
});

function showResults(results) {
	var resultsElement = document.getElementById('results');
	resultsElement.innerText = results;
}

function savekey() {
	showResults("");
	var inputkey = document.getElementById('pvkey');
	var accname = document.getElementById('account');
	// test key WIF format
	var WIFregex = /5[HJK][1-9A-HJ-NP-Za-km-z]{49}/;
	WIFkey = inputkey.value.match(WIFregex);
	if ( WIFkey == null ){
		showResults("Error: invalid key");
		return;
	}
	function saved(){
		dispname(accname.value);
		hideinput();
	}
	// ToDo : client-side test with a get_account query that this account name
	//         - exists
	//         - has this address registered for eospass key
	chrome.storage.local.set({'pvKey':WIFkey[0],'account':accname.value}, saved);
}

function removekey() {
		// delete all user data
		chrome.storage.local.clear(showinput);
}

function dispname(name) {
	var domname = document.getElementById('accname');
	domname.innerText = name;
}

function hideinput() {
	document.getElementById('pvkey').value = "";
	document.getElementById('account').value = "";
	document.getElementById('inputkey').style.display = "none";
	document.getElementById('userkey').style.display = "inherit";
}

function showinput() {
	document.getElementById('inputkey').style.display = "inherit";
	document.getElementById('userkey').style.display = "none";
}
